import datetime


def sumarHora(horaActual):
    return int(horaActual) + 2;
    
    
def mostrarResultado(hora,minutos, amOrPm):
    print("La hora es: ",hora,":",minutos,amOrPm)
    
    
def currentTime():
    horaActual = datetime.datetime.now().strftime("%H")
    minutos = datetime.datetime.now().strftime("%M")
    amOrPm = datetime.datetime.now().strftime("%p")
    
    mostrarResultado(sumarHora(horaActual),minutos, amOrPm)
    
    
    
