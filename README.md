## Ejecicios con Python

Para jugar con los ejercicios se ejecuta en consola el comando...

``
python main.py

``
Los ejecicios que se resolvieron son los siguientes:

1. Escribe un programa Python que acepte el radio de un círculo del usuario y calcule el área.
2. Escribe un programa Python que acepte un número entero (n) y calcule el valor de n + nn + nnn
3. Escribe un programa en Python que acepte una cadena de caracteres y cuente el tamaño de la cadena y cuantas veces aparece la letra A (mayuscula y minúscula)
4. Escribe un programa en Python que muestre la hora actual con una suma de dos horas adicionales