import area
import letraEnCaracter
import numeroEntero
import horaActual

s = ''

while(s != "s"):
       
    #Ménu
    print("1. Cálcular el área de un circulo\n")
    print("2. Cálcular el valor de n + nn + nnn\n")
    print("3. Cuántas letras aparece en un texto\n")
    print("4. Hora actual más dos\n")

    #Pedir datos al usuario
    e = int(input("¿Qué ejercicio desea hacer? "))
    
    
    #Evaluar opción del usuario
    if e == 1:
        area.pedirDatos()
    if e == 2:
        numeroEntero.pedirDatos()
    if e == 3:
        letrasEnCaracter.pedirDatos()
    if e == 4:
        horaActual.currentTime()
    

    s = input("¿Desea salir? s/n\n")



