from math import pi


def calcularArea(radio):
    resultado = pi* radio**2

    #Imprimir el resultado con ayuda de Unicode
    print(round(resultado, 2),u"cm\xb2") 
    
    
def pedirDatos():
    radio = int(input("Ingresa el radio: "))
    calcularArea(radio)
    
